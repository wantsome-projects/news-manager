#https://www.timesnewroman.ro/?act=rss
#https://www.reddit.com/.rss

from datetime import datetime
from datetime import timedelta
import time
import feedparser
from dateutil import parser

sursa = feedparser.parse("https://www.reddit.com/.rss")
#sursa = feedparser.parse(input('Sursa? : '))

ore = int(input('Cate ore de pauza? ' ))
minute = int(input('Cate minute de pauza? ' ))
secunde = int(input('Cate secunde de pauza? '))

last24 = datetime.now() - timedelta(hours = 24)
last24_format = datetime.strftime(last24, "%d/%m/%Y - %H:%M:%S")

def afisare():

  for sursaentry in sursa.entries:

    data_adaugare = sursaentry.get("updated")
    adaugare_parsat = parser.parse(data_adaugare)
    adaugare_formatat = datetime.strftime(adaugare_parsat, "%d/%m/%Y - %H:%M:%S")

    if adaugare_formatat > str(last24_format):

      print(' ')
      print(' ')
      print("*************************")
      print()
      print('Titlu: ', sursaentry.get("title", ""))
      print()
      print('Link: ', sursaentry.get("link", ""))
      print()
      print('Ultima actualizare: ', sursaentry.get("updated"))
      print("-------------------------")
      print(' ')

while sursa:

  print()
  print("Se iau ultimele stiri pe bune de pe", sursa.feed.get("title"), '...')
  print()

  afisare()
  
  #print('Se asteapta %d ore, %d minute si %d secunde..' % ore % minute % secunde)
  time.sleep(ore * 3600 + minute * 60 + secunde)

  afisare()